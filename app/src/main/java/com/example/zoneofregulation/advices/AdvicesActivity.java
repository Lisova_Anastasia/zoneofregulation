package com.example.zoneofregulation.advices;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zoneofregulation.Database;
import com.example.zoneofregulation.R;
import com.example.zoneofregulation.diary.Diary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class AdvicesActivity extends AppCompatActivity {
    private SQLiteDatabase mDb;
    LinearLayout layout;
    Random random = new Random();
    BufferedWriter writer;
    BufferedReader reader;
    List<String> advicesList = new ArrayList<>();
    TextView advice;
    String key;
    Button btnNextAdvice;
    Date currentDate;
    DateFormat dateFormat;
    DateFormat timeFormat;
    String dateTime;
    EditText diaryTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advices);
        layout = findViewById(R.id.diaryLayout);
        diaryTxt = findViewById(R.id.diaryTxt);

        Database mDBHelper = new Database(this);
        mDBHelper.updateDataBase();
        mDb = mDBHelper.getWritableDatabase();

        Intent intent = getIntent();
        key = intent.getStringExtra("file");
        String advices = "";

        try {
//            writeToFile(key, advices);
            readFile(key);
            addAdvices(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        advice = findViewById(R.id.advice);
        printFirstAdvice();
    }

    /**
     * Метод производит запись в файл
     * советов
     *
     * @param file    файл
     * @param advices советы
     * @throws IOException исключение
     */
    private void writeToFile(String file, String advices) throws IOException {
        writer = new BufferedWriter(new OutputStreamWriter(openFileOutput(file, MODE_PRIVATE)));
        writer.write(advices);
        writer.close();
    }

    /**
     * Метод производит чтение данных из файла
     *
     * @param file файл
     * @throws FileNotFoundException исключение
     */
    private void readFile(String file) throws FileNotFoundException {
        reader = new BufferedReader(new InputStreamReader(openFileInput(file)));
    }

    /**
     * Метод для добавление советов из
     * прочитанного файла
     *
     * @param reader прочитанный файл
     * @throws IOException исключение
     */
    private void addAdvices(BufferedReader reader) throws IOException {
        String str;
        while ((str = reader.readLine()) != null) {
            advicesList.add(str);
        }
    }

    /**
     * Метод выводит первый совет
     */
    private void printFirstAdvice() {
        int index = random.nextInt(advicesList.size());
        advice.setText(String.valueOf(advicesList.get(index)));
        advicesList.remove(index);
    }

    /**
     * Метод выводит все последующие
     * советы, пока они не закончатся
     */
    public void nextAdvice(View view) {
        printFirstAdvice();

        if (advicesList.size() == 0) {
            advice.setText("Советы закончились");
            btnNextAdvice.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Возвращает дату и время
     * с телефона пользователя
     *
     * @return дату и время
     */
    private String getDateTime() {
        currentDate = new Date();
        dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        dateTime = dateFormat.format(currentDate) + '\n' + timeFormat.format(currentDate);
        return dateTime;
    }

    /**
     * Метод открывает панель
     * для записи в дневник
     */
    public void writeToDiary(View view) {
        layout.setVisibility(View.VISIBLE);
    }

    /**
     * Метод сохраняет записанный текст
     * в дневник (базу данных)
     */
    public void saveDiary(View view) {
        if (diaryTxt.getText().toString().isEmpty()) {
            diaryTxt.setText("Напиши о своих чувствах");
            return;
        }
        switch (key) {
            case "RedAdvices.txt":
                addEntry("Красная зона");
                break;
            case "YellowAdvices.txt":
                addEntry("Жёлая зона");
                break;
            case "BlueAdvices.txt":
                addEntry("Синяя зона");
                break;
            case "GreenAdvices.txt":
                addEntry("Зелёная зона");
                break;
        }
    }

    /**
     * Добавляет запись в дневник
     * и статистику для родителей
     *
     * @param emotional эмоция
     */
    private void addEntry(String emotional) {
        mDb.execSQL("INSERT INTO diary (date_time, diary, emotional) VALUES ('" + getDateTime() + "','" + diaryTxt.getText() + "','" + emotional + "')");
        mDb.execSQL("INSERT INTO status (date_time, emotional) VALUES ('" + getDateTime() + "','" + emotional + "')");
        Toast.makeText(this, "Запись добавлена!", Toast.LENGTH_LONG).show();
        diaryTxt.setText("");
    }

    /**
     * Открывает дневник (базу данных)
     * для чтения
     */
    public void openDiary(View view) {
        Intent intent = new Intent(this, Diary.class);
        startActivity(intent);
    }
}
