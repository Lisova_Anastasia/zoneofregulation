package com.example.zoneofregulation.advices;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.zoneofregulation.R;

public class ZoneOfRegulation extends AppCompatActivity {
    Intent intent;
    private final String RED_ADVICES_FILE = "RedAdvices.txt";
    private final String BLUE_ADVICES_FILE = "BlueAdvices.txt";
    private final String YELLOW_ADVICES_FILE = "YellowAdvices.txt";
    private final String GREEN_ADVICES_FILE = "GreenAdvices.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_of_regulation);
        intent = new Intent(this, AdvicesActivity.class);
    }

    public void onClickBtnRed(View view) {
        intent.putExtra("file", RED_ADVICES_FILE);
        startActivity(intent);
    }

    public void onClickBtnGreen(View view) {
        intent.putExtra("file", GREEN_ADVICES_FILE);
        startActivity(intent);
    }

    public void onClickBtnBlue(View view) {
        intent.putExtra("file", BLUE_ADVICES_FILE);
        startActivity(intent);
    }

    public void onClickBtnYellow(View view) {
        intent.putExtra("file", YELLOW_ADVICES_FILE);
        startActivity(intent);
    }
}
