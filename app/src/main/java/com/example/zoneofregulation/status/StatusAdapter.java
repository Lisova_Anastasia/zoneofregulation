package com.example.zoneofregulation.status;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zoneofregulation.Database;
import com.example.zoneofregulation.R;

import java.util.ArrayList;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder> {
    private Cursor cursor;
    private ArrayList<Integer> IDs;

    StatusAdapter(Context context, Cursor cursor) {
        this.cursor = cursor;
        Database mDBHelper = new Database(context);
        mDBHelper.updateDataBase();

        cursor.moveToFirst();
        IDs = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell, parent, false);
        return new StatusAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        if (cursor != null && !cursor.isAfterLast()) {
            fillData(viewHolder);
            IDs.add(Integer.valueOf(cursor.getString(0)));
            cursor.moveToNext();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.entry);
        }
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    @SuppressLint("SetTextI18n")
    private void fillData(ViewHolder viewHolder) {
        viewHolder.title.setText(cursor.getString(1) + '\n' + cursor.getString(2));
    }
}
