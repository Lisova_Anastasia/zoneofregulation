package com.example.zoneofregulation.status;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import com.example.zoneofregulation.Database;
import com.example.zoneofregulation.R;

public class EmotionalStatus extends AppCompatActivity {
    Cursor cursor;
    private SQLiteDatabase mDb;
    RecyclerView recyclerView;
    StatusAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emotional_status);

        Database mDBHelper = new Database(this);
        mDBHelper.updateDataBase();
        mDb = mDBHelper.getWritableDatabase();

        recyclerView = findViewById(R.id.statusList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        cursor = mDb.rawQuery("SELECT * FROM status ORDER BY id DESC", null);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        adapter = new StatusAdapter(this, cursor);
        recyclerView.setAdapter(adapter);
    }
}
