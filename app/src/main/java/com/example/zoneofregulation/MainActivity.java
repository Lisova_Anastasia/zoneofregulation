package com.example.zoneofregulation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.zoneofregulation.advices.ZoneOfRegulation;
import com.example.zoneofregulation.status.EmotionalStatus;

/**
 * Класс для представления
 * окна авторизация
 *
 * @author Лисова Анастасия
 */
public class MainActivity extends AppCompatActivity {
    private final String USER_MODE = "SELECT * FROM users WHERE role = 'USER_KID'";
    private final String PARENT_MODE = "SELECT * FROM users WHERE role = 'USER_PARENT'";

    Database mDBHelper;
    Cursor cursor;
    private SQLiteDatabase mDb;
    EditText login;
    EditText password;
    RadioButton userMode;
    RadioButton parentMode;
    RadioGroup modes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDBHelper = new Database(this);
        mDBHelper.updateDataBase();
        mDb = mDBHelper.getWritableDatabase();

        login = findViewById(R.id.login);
        password = findViewById(R.id.password);
        userMode = findViewById(R.id.userMode);
        parentMode = findViewById(R.id.parentMode);
        modes = findViewById(R.id.modes);
    }

    /**
     * Обрабатывает нажатие
     * на кнопку "Войти"
     */
    public void onClickBtnLogin(View view) {
        if (login.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
            Toast.makeText(this, "Заполните поля ввода!", Toast.LENGTH_LONG).show();
            return;
        }

        switch (modes.getCheckedRadioButtonId()) {
            case R.id.userMode:
                cursor = mDb.rawQuery(USER_MODE, null);
                if(isCheckLogin()){
                    Intent intent = new Intent(this, ZoneOfRegulation.class);
                    startActivity(intent);
                }
                break;
            case R.id.parentMode:
                cursor = mDb.rawQuery(PARENT_MODE, null);
                if(isCheckLogin()){
                    Intent intent = new Intent(this, EmotionalStatus.class);
                    startActivity(intent);
                }
                break;
            default:
                Toast.makeText(this, "Выберите режим!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Выводит надписи при попытке
     * входа в аккаунт и возвращает true,
     * если все данные верные, иначе false
     *
     * @return true при верных данных, иначе false
     */
    private boolean isCheckLogin() {
        if (cursor != null && cursor.moveToFirst()) {
            if (isTrueData()) {
                Toast.makeText(this, "Добро пожаловать!", Toast.LENGTH_LONG).show();
                return true;
            } else {
                Toast.makeText(this, "Неверные данные!", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        cursor.close();
        return false;
    }

    /**
     * Возвращает true, если введённые
     * логин и пароль совпадают с теми,
     * что есть в базе данных, иначе false
     *
     * @return true при верных данных, иначе false
     */
    private boolean isTrueData() {
        return cursor.getString(1).equals(login.getText().toString()) &&
                cursor.getString(2).equals(password.getText().toString());
    }
}
